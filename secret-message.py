#!/usr/bin/python3

from PIL import Image
from enum import Enum

class Action(Enum):
    START_UP = 0
    START_LEFT = 1
    MOVE_UP = 2
    MOVE_RIGHT = 3
    MOVE_DOWN = 4
    MOVE_LEFT = 5
    TURN_LEFT = 6
    TURN_RIGHT = 7
    STOP = 8
    CONTINUE = 9

class Rule():
    START_UP = (7, 84, 19)       # Start drawing upwards when the pixel color is 7, 84, 19.
    START_LEFT = (139, 57, 137)  # Start drawing left when the pixel color is 139, 57, 137.
    STOP = (51, 69, 169)         # Stop drawing when the pixel color is 51, 69, 169.
    TURN_LEFT = (123, 131, 154)  # Turn left when the pixel color is 123, 131, 154.
    TURN_RIGHT = (182, 149, 72)  # Turn right when the pixel color is 182, 149, 72.

def check_pixel(x, y, starting_points, instructions, pixels):
    if pixels[x, y] == Rule.START_UP:
        starting_points[Action.START_UP].append({'y': y, 'x': x})
        instructions[y][x] = Action.MOVE_UP
    elif pixels[x, y] == Rule.START_LEFT:
        starting_points[Action.START_LEFT].append({'y': y, 'x': x})
        instructions[y][x] = Action.MOVE_LEFT
    elif pixels[x, y] == Rule.STOP:
        instructions[y][x] = Action.STOP
    elif pixels[x, y] == Rule.TURN_LEFT:
        instructions[y][x] = Action.TURN_LEFT
    elif pixels[x, y] == Rule.TURN_RIGHT:
        instructions[y][x] = Action.TURN_RIGHT

def draw(x, y, current_direction, instructions, pixels):
    # Stop if we're out of image bounds.
    if x < 0 or y < 0 or x >= len(instructions[0]) or y >= len(instructions):
        return
    print(y, x, current_direction, instructions[y][x])

    # Color the current pixel black.
    pixels[x, y] = (0, 0, 0)

    current_action = instructions[y][x]

    # Decide the next action.
    if current_action == Action.STOP:
        return
    # Check if we need to turn left.
    elif current_action == Action.TURN_LEFT:
        if current_direction == Action.MOVE_UP:
            current_direction = Action.MOVE_LEFT
        elif current_direction == Action.MOVE_RIGHT:
            current_direction = Action.MOVE_UP
        elif current_direction == Action.MOVE_DOWN:
            current_direction = Action.MOVE_RIGHT
        elif current_direction == Action.MOVE_LEFT:
            current_direction = Action.MOVE_DOWN
    # Check if we need to turn right.
    elif current_action == Action.TURN_RIGHT:
        if current_direction == Action.MOVE_UP:
            current_direction = Action.MOVE_RIGHT
        elif current_direction == Action.MOVE_RIGHT:
            current_direction = Action.MOVE_DOWN
        elif current_direction == Action.MOVE_DOWN:
            current_direction = Action.MOVE_LEFT
        elif current_direction == Action.MOVE_LEFT:
            current_direction = Action.MOVE_UP
    elif current_action == Action.CONTINUE:
        instructions[y][x] = current_direction
        #return

    # Move.
    if current_direction == Action.MOVE_UP:
        y -= 1
    elif current_direction == Action.MOVE_DOWN:
        y += 1
    elif current_direction == Action.MOVE_RIGHT:
        x += 1
    elif current_direction == Action.MOVE_LEFT:
        x -= 1

    draw(x, y, current_direction, instructions, pixels)

def main():
    # Load the secret image.
    img = Image.open('secret.png') # Can be many different formats.
    pixels = img.load()

    # Get the pixel width and height of the image.
    img_width, img_height = img.size

    # Initialize a dictionary for actions per pixel.
    starting_points = {
      Action.START_UP: [],
      Action.START_LEFT: []
    }
    instructions = [[Action.CONTINUE for x in range(img_width)] for y in range(img_height)]

    current_direction = None # Needed for turning.

    for y in range(img_height):
        for x in range(img_width):
           check_pixel(x, y, starting_points, instructions, pixels)

    # Start drawing from starting points.
    for starting_point in starting_points[Action.START_UP]:
        current_direction = Action.MOVE_UP
        y, x = starting_point.values()
        draw(x, y, current_direction, instructions, pixels)
    for starting_point in starting_points[Action.START_LEFT]:
        current_direction = Action.MOVE_LEFT
        y, x = starting_point.values()
        draw(x, y, instructions[y][x], instructions, pixels)
    img.save('decoded.png')  # Save the modified pixels as .png+

main()
